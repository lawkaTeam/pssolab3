package psso.lab3;

import java.util.Calendar;

public final class Lab3Util {
	
	private Lab3Util() {}
	
	final public static long getCurrentTime() {
		return Calendar.getInstance().getTimeInMillis();
	}
	
	final public static long getSecondsBetweenTime(long start, long stop) {
		return (stop - start) / 1000;
	}
}
