package psso.lab3.oo.connection;

import psso.lab3.oo.customer.TimingCustomer;

public class TimingLongDistance extends TimingConnection {

	final private static int LONG_DISTANCE_RATE = 10;
	
	public TimingLongDistance(TimingCustomer a, TimingCustomer b) {
		super(a, b);
	}
	
	@Override
	protected int getRate() {
		return LONG_DISTANCE_RATE;
	}
}
