package psso.lab3.oo.connection;

import psso.lab3.oo.customer.TimingCustomer;

public class TimingLocal extends TimingConnection {

	final private static int LOCAL_RATE = 3;
	
	public TimingLocal(TimingCustomer a, TimingCustomer b) {
		super(a, b);
	}

	@Override
	protected int getRate() {
		return LOCAL_RATE;
	}
}
