package psso.lab3.oo.call;

import java.util.Vector;

import psso.lab3.base.call.Call;
import psso.lab3.base.connection.Connection;
import psso.lab3.oo.connection.TimingLocal;
import psso.lab3.oo.connection.TimingLongDistance;
import psso.lab3.oo.customer.TimingCustomer;

public class TimingCall extends Call {
	
	public TimingCall(TimingCustomer caller, TimingCustomer receiver) {
		super(caller, receiver);
		connections = new Vector<Connection>(); // cleanup
		
		Connection c;
		if (receiver.localTo(caller)) {
			c = new TimingLocal(caller, receiver);
		} else {
			c = new TimingLongDistance(caller, receiver);
		}
		connections.addElement(c);
	}
}
