/*

Copyright (c) Xerox Corporation 1998-2002.  All rights reserved.

Use and copying of this software and preparation of derivative works based
upon this software are permitted.  Any distribution of this software or
derivative works must comply with all applicable United States export control
laws.

This software is made available AS IS, and Xerox Corporation makes no warranty
about the software, its performance or its conformity to any specification.

|<---            this code is formatted to fit into 80 columns             --->|
|<---            this code is formatted to fit into 80 columns             --->|
|<---            this code is formatted to fit into 80 columns             --->|

*/
package psso.lab3.oo;

import psso.lab3.base.AbstractSimulation;
import psso.lab3.base.call.Call;
import psso.lab3.base.customer.Customer;
import psso.lab3.oo.customer.TimingCustomer;

/**
 * This simulation subclass implements AbstractSimulation.run(..)
 * with  a test script for the telecom system with only the
 * basic objects.
 */
public class OoSimulation extends AbstractSimulation {

    public static void main(String[] args){
    	simulation = new OoSimulation();
    	simulation.run();
    }
    
    @Override
    public void run() {
		TimingCustomer jim = new TimingCustomer("Jim", 650);
		TimingCustomer mik = new TimingCustomer("Mik", 650);
		TimingCustomer crista = new TimingCustomer("Crista", 415);

		say("jim calls mik...");
		Call c1 = jim.call(mik);
		wait(1);
		say("mik accepts...");
		mik.pickup(c1);
		wait(4);
		say("jim hangs up...");
		jim.hangup(c1);

		say("jim calls crista...");
		Call c2 = jim.call(crista);
		say("crista accepts...");
		crista.pickup(c2);
		wait(2);
		say("crista hangs up...");
		crista.hangup(c2);

		report(jim);
		report(crista);
		report(mik);
    }

	@Override
    protected void report(Customer c) { 
		TimingCustomer timingCustomer = (TimingCustomer) c;
		say(
			c.toString() 
			+ " has been connected for "
			+ timingCustomer.getTotalConnectTime()
			+ " seconds and has a bill of "
			+ timingCustomer.getTotalCharge()
		);
	}
}
