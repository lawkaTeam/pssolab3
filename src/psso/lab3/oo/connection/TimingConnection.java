package psso.lab3.oo.connection;

import psso.lab3.Lab3Util;
import psso.lab3.base.connection.Connection;
import psso.lab3.base.customer.Customer;
import psso.lab3.oo.customer.TimingCustomer;

public abstract class TimingConnection extends Connection {
	
	protected long startTime;
	
	protected long stopTime;
	
	protected TimingConnection(Customer a, Customer b) {
		super(a, b);
		startTime = stopTime = Lab3Util.getCurrentTime();
	}
	
	protected abstract int getRate();
	
	@Override
	public void complete() {
		super.complete();
		startTime = stopTime = Lab3Util.getCurrentTime();
	}
	
	@Override
	public void drop() {
		super.drop();
		stopTime = Lab3Util.getCurrentTime();
		long seconds = Lab3Util.getSecondsBetweenTime(startTime, stopTime);
		long charge = seconds * getRate();
		
		((TimingCustomer) getReceiver()).addConnectTime(seconds);
		((TimingCustomer) getCaller()).addConnectTime(seconds);
		((TimingCustomer) getCaller()).addConnectCharge(charge);
	}
}
