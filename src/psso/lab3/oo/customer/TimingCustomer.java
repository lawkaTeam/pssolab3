package psso.lab3.oo.customer;

import psso.lab3.base.call.Call;
import psso.lab3.base.customer.Customer;
import psso.lab3.oo.call.TimingCall;

public class TimingCustomer extends Customer {

	protected long totalConnectTime;
	
	protected long totalCharge;
	
	public TimingCustomer(String name, int areacode) {
		super(name, areacode);
		totalConnectTime = 0;
	}
	
	public Call call(TimingCustomer receiver) {
		TimingCall call = new TimingCall(this, receiver);
		addCall(call);
		return call;
	}
	
	public void addConnectTime(long time) {
		totalConnectTime += time;
	}
	
	public void addConnectCharge(long charge) {
		totalCharge += charge;
	}
	
	public long getTotalConnectTime() {
		return totalConnectTime;
	}
	
	public long getTotalCharge() {
		return totalCharge;
	}
}
