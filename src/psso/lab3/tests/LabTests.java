package psso.lab3.tests;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import psso.lab3.base.BasicSimulation;
import psso.lab3.oo.OoSimulation;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class LabTests {

	private ByteArrayOutputStream out;
	
	private final String EXTENDED_RESULT;
	
	private final String BASIC_RESULT;
	
	public LabTests() {
		BASIC_RESULT = readFile("basic.txt");
		EXTENDED_RESULT = readFile("extended.txt");
	}
	
	@Before
	public void setUp() {
		out = new ByteArrayOutputStream();
		System.setOut(new PrintStream(out));
	}
	
	@Test
	public void basicSimulation() {
		BasicSimulation.main(null);
		Assert.assertEquals(BASIC_RESULT, readOutput());
	}
	
	@Test
	public void ooSimulation() {
		OoSimulation.main(null);
		Assert.assertEquals(EXTENDED_RESULT, readOutput());
	}

	private String readOutput() {
		return new String(out.toString());
	}
	
	private String readFile(String fileName) {
		try {
			return new String(Files.readAllBytes(Paths.get(fileName)));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}	
}
